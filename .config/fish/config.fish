# Bootstrap installation of fisher packeges
if not functions -q fisher
    set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME ~/.config
    curl https://git.io/fisher --create-dirs -sLo $XDG_CONFIG_HOME/fish/functions/fisher.fish
    fish -c fisher
end

# Aliases
alias vim="nvim"
alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"

# Variables

# Set path
set -x PATH '/home/bondzula/bin' $PATH
set -x PATH '/home/bondzula/.emacs.d/bin' $PATH
set -x PATH '/home/bondzula/.config/composer/vendor/bin' $PATH
set -x PATH '/home/bondzula/.nvm/versions/node/v12.11.0/bin' $PATH

# Set editor
set -Ux EDITOR emacs

set -x LIBVA_DRIVER_NAME 'iHD'
set -x QT_QPA_PLATFORM 'wayland-egl'

set -U FZF_LEGACY_KEYBINDINGS 0

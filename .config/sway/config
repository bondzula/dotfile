# i3 configuration file (v4)

# Windows setup
default_border none
default_floating_border none

gaps inner 2
smart_gaps on

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier Mod4

# start a terminal
# bindsym Mod4+Return exec st
bindsym Mod4+Return exec alacritty
#
# Make the currently focused window a scratchpad
bindsym Mod4+Shift+minus move scratchpad

# Show the first scratchpad window
bindsym Mod4+minus scratchpad show

# start terminal with tmux
# bindsym Mod4+Shift+Return exec st -e tmux
bindsym Mod4+Shift+Return exec alacritty -e tmux

# Application launcher
bindsym Mod4+d exec ulauncher-toggle

# kill focused window
bindsym Mod4+Shift+q kill

# change focus
bindsym Mod4+h focus left
bindsym Mod4+j focus down
bindsym Mod4+k focus up
bindsym Mod4+l focus right

# move focused window
bindsym Mod4+Shift+h move left
bindsym Mod4+Shift+j move down
bindsym Mod4+Shift+k move up
bindsym Mod4+Shift+l move right

# toggle split direction
bindsym Mod4+v split toggle

# enter fullscreen mode for the focused container
bindsym Mod4+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym Mod4+s layout stacking
bindsym Mod4+w layout tabbed
bindsym Mod4+a layout toggle split

# toggle tiling / floating
bindsym Mod4+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym Mod4+space focus mode_toggle

# External monitor side
# output eDP-1 pos 1920 0 res 1920x1080
# output HDMI-A-1 pos 0 0 res 1920x1080
output eDP-1 pos 0 0 res 1920x1080
output HDMI-A-1 pos 1920 0 res 1920x1080

# Assign workspace to monitor
# workspace 6 output eDP-1
# workspace 7 output eDP-1
# workspace 8 output eDP-1
# workspace 9 output eDP-1
# workspace 10 output eDP-1

# workspace 1 output HDMI-A-1
# workspace 2 output HDMI-A-1
# workspace 3 output HDMI-A-1
# workspace 4 output HDMI-A-1
# workspace 5 output HDMI-A-1

workspace 1 output eDP-1
workspace 2 output eDP-1
workspace 3 output eDP-1
workspace 4 output eDP-1
workspace 5 output eDP-1

workspace 6 output HDMI-A-1
workspace 7 output HDMI-A-1
workspace 8 output HDMI-A-1
workspace 9 output HDMI-A-1
workspace 10 output HDMI-A-1

# switch to workspace
bindsym Mod4+1 workspace 1
bindsym Mod4+2 workspace 2
bindsym Mod4+3 workspace 3
bindsym Mod4+4 workspace 4
bindsym Mod4+5 workspace 5
bindsym Mod4+6 workspace 6
bindsym Mod4+7 workspace 7
bindsym Mod4+8 workspace 8
bindsym Mod4+9 workspace 9
bindsym Mod4+0 workspace 10
bindsym Mod4+bracketright workspace next
bindsym Mod4+bracketleft workspace prev

#Go to last workspace etc
workspace_auto_back_and_forth yes

# move focused container to workspace
bindsym Mod4+Shift+1 move container to workspace 1
bindsym Mod4+Shift+2 move container to workspace 2
bindsym Mod4+Shift+3 move container to workspace 3
bindsym Mod4+Shift+4 move container to workspace 4
bindsym Mod4+Shift+5 move container to workspace 5
bindsym Mod4+Shift+6 move container to workspace 6
bindsym Mod4+Shift+7 move container to workspace 7
bindsym Mod4+Shift+8 move container to workspace 8
bindsym Mod4+Shift+9 move container to workspace 9
bindsym Mod4+Shift+0 move container to workspace 10

# reload the configuration file
bindsym Mod4+Shift+c reload

# power management shortcuts
bindsym Mod4+q mode "Power-mode"
mode "Power-mode" {
    bindsym l exec betterlockscreen -l dim, mode "default"
    bindsym e exec swaymsg exit, mode "default"
    bindsym r exec systemctl reboot, mode "default"
    bindsym s exec systemctl suspend, mode "default"
    bindsym h exec systemctl hybrid-sleep, mode "default"
    bindsym p exec systemctl poweroff -i, mode "default"  

    # back to normal mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Resize mode
bindsym Mod4+r mode "Resize-mode"
mode "Resize-mode" {
    bindsym l resize shrink width 10 px or 10 ppt
    bindsym j resize grow height 10 px or 10 ppt
    bindsym k resize shrink height 10 px or 10 ppt
    bindsym h resize grow width 10 px or 10 ppt

    # back to normal mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Music mode TODO - integrate some feed back
set $mpc mpc -p 6600
bindsym Mod4+m  mode "Music-mode"
mode "Music-mode" {
    bindsym n exec $mpc next, mode "default"
    bindsym p exec $mpc prev, mode "default"
    bindsym r exec $mpc repeat, mode "default"
    bindsym t exec $mpc toggle, mode "default"
    bindsym m exec $mpc toggleoutput 1, mode "default"
    bindsym s exec $mpc stop, mode "default"
    bindsym a exec $mpc shuffle, mode "default"

    # back to normal mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Emacs mode
bindsym Mod4+e mode "Emacs-mode"
mode "Emacs-mode" {
     bindsym e exec emacs, mode "default"
     bindsym c exec emacsclient -c, mode "default"
     bindsym n exec org-capture, mode "default"

     bindsym Return mode "default"
     bindsym Escape mode "default"
}

bindsym Mod4+c exec code

# Toggle sticy on floating windows
bindsym Mod4+Shift+s sticky toggle

# Go to last visited workspace
bindsym Mod4+z workspace back_and_forth

# Send windows to last visited workspace
bindsym Mod4+Shift+z move container to workspace back_and_forth; workspace back_and_forth

for_window [window_role="pop-up"] floating enable
for_window [window_role="task_dialog"] floating enable
for_window [class="st-256color"] floating disable
assign [class="glip-nativefier-38fda9"] 10

for_window [app_id="ulauncher"] floating enable

# Dont lock if fullscreen is detected
for_window [class="Google-chrome"] inhibit_idle fullscreen
for_window [class="Firefox"] inhibit_idle fullscreen
for_window [class="mpv"] inhibit_idle fullscreen

#VOLUME
bindsym XF86AudioRaiseVolume exec pamixer -i 5 && volnoti-show `pamixer --get-volume`
bindsym XF86AudioLowerVolume exec pamixer -d 5 && volnoti-show `pamixer --get-volume`
bindsym XF86AudioMute exec pamixer -t && volnoti-show `pamixer --get-volume`

#Brightness
bindsym XF86MonBrightnessDown exec light -U 10
bindsym XF86MonBrightnessUp exec light -A 10

# screenshot
bindsym Print exec --no-startup-id grim -o $(swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name') $HOME/Photos/Screenshots/$(date +'%Y-%m-%d-%H%M%S_grim.png')
bindsym Ctrl+Print exec --no-startup-id grim -g "$(slurp)" $HOME/Photos/Screenshots/$(date +'%Y-%m-%d-%H%M%S_grim.png')
bindsym Shift+Print exec --no-startup-id grim $HOME/Photos/Screenshots/$(date +'%Y-%m-%d-%H%M%S_grim.png')

output "*" background /home/bondzula/Photos/background.* fill

# exec --no-startup-id dropbox &
exec --no-startup-id ulauncher --hide-window &
exec --no-startup-id mako &
exec --no-startup-id udiskie &
exec /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1

exec swayidle -w \
    timeout 600 'swaylock' \
    timeout 1200 'swaymsg "output * dpms off"' \
        resume 'swaymsg "output * dpms on"' \
    before-sleep 'swaylock'

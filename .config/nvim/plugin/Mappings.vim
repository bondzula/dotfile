"Quits and writes files             
nnoremap <Leader>q :q<CR>
nnoremap <Leader>w :up<CR>

"======================= Movement ==========================

"Disable arrow keys xmode enter help with F1
nnoremap <up> <NOP>
nnoremap <down> <NOP>
nnoremap <left> <NOP>
nnoremap <right> <NOP>
nnoremap <bs> <NOP>
nnoremap <delete> <NOP>
inoremap <up> <NOP>
inoremap <down> <NOP>
inoremap <left> <NOP>
inoremap <right> <NOP>

" line wise up and down by default
nnoremap j gj
nnoremap k gk
nnoremap gj 5j
nnoremap gk 5k
vnoremap j gj
vnoremap k gk
vnoremap gj 5j
vnoremap gk 5k

"Use :norm! to make it count as one command
nnoremap <silent> n :norm! nzz<CR>
nnoremap <silent> N :norm! Nzz<CR>

" Same when moving up and down
nnoremap <C-u> <C-u>zz
nnoremap <C-d> <C-d>zz
nnoremap <C-f> <C-f>zz
nnoremap <C-b> <C-b>zz

" More logical Y
nnoremap Y y$

"======================= Search ====================================

"Remove highlighting of previous search
nnoremap <silent> <BS> :nohl<cr>
nnoremap S :%s//g<Left><Left>
nnoremap <A-R> :%s/<C-r><C-w>//g<Left><Left>
nnoremap <A-r> :s/<C-r><C-w>//g<Left><Left>

" ====================== F keys ====================================

nnoremap <silent> <F9> :set spell!<Cr>
inoremap <silent> <F9> <C-o>:set spell<Cr>

nnoremap <silent> <F10> :set number!<Cr>
inoremap <silent> <F10> <C-o>:set number!<Cr>

" Keep the cursor in place while joining lines
nnoremap J mzJ`z

" Visual shifting 
vnoremap < <gv
vnoremap > >gv

" Allow using the repeat operator with a visual selection
vnoremap . :normal .<CR>

" Move visual blockC 
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" correct previous misspelling
" inoremap <C-s> <c-g>u<Esc>[s1z=`]A<c-g>u
"
"Swap normal visual with block visual
nnoremap    v    <C-V>
nnoremap  <C-V>    v

"~ EasyAlign
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)


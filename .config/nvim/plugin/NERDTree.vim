"Vim plugin file
"Maintainer: Stefan Bondzulic
"Last Edit: 2017/07/26


    " )                                    
 " ( /(           (     *   )              
 " )\())  (  (    )\ )` )  /((     (   (   
" ((_)\  ))\ )(  (()/( ( )(_))(   ))\ ))\  
 " _((_)/((_|()\  ((_)|_(_()|()\ /((_)((_) 
" | \| (_))  ((_) _| ||_   _|((_|_))(_))   
" | .` / -_)| '_/ _` |  | | | '_/ -_) -_)  
" |_|\_\___||_| \__,_|  |_| |_| \___\___|  
                                         


"Delete a buffer when file is deleted with NERDTree
let NERDTreeAutoDeleteBuffer = 1
let NERDTreeRespectWildIgnor = 1

"Make it look nicer
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let g:NERDTreeWinSize = 30
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

" ====================== Mappings =========================

nnoremap <silent> <F1> :NERDTreeToggle<CR>

"Vim plugin file
"Maintainer: Stefan Bondzulic
"Last Edit: 2017/07/26


"  (                               
"  )\ )    (  (                    
" (()/(    )\ )\ ) (        (  (   
"  /(_))( ((_|()/( )\  (    )\))(  
" (_))_|)\ _  ((_)|(_) )\ )((_))\  
" | |_ ((_) | _| | (_)_(_/( (()(_) 
" | __/ _ \ / _` | | | ' \)) _` |  
" |_| \___/_\__,_| |_|_||_|\__, |  
"                          |___/   


"========== Folding ===========

set foldtext=MyFoldText()
function! MyFoldText()
  let nucolwidth = &fdc + &number*&numberwidth
  let winwd = winwidth(0) - nucolwidth - 5
  let foldlinecount = foldclosedend(v:foldstart) - foldclosed(v:foldstart) + 1
  let fdnfo = '  '. string(foldlinecount) . '  '
  let line =  strpart(getline(v:foldstart), 0 , winwd - len(fdnfo))
  let fillcharcount = winwd - len(line) - len(fdnfo)
  return line . repeat(" ",fillcharcount) . fdnfo
endfunction

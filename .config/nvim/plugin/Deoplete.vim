"Vim plugin file
"Maintainer: Stefan Bondzulic
"Last Edit: 2017/07/26


 " (           )  (   (                       
 " )\ )     ( /(  )\ ))\ )   (      *   )     
" (()/(  (  )\())(()/(()/(   )\   ` )  /((    
 " /(_)) )\((_)\  /(_))(_)|(((_)(  ( )(_))\   
" (_))_ ((_) ((_)(_))(_))  )\ _ )\(_(_()|(_)  
 " |   \| __/ _ \| _ \ |   (_)_\(_)_   _| __| 
 " | |) | _| (_) |  _/ |__  / _ \   | | | _|  
 " |___/|___\___/|_| |____|/_/ \_\  |_| |___| 
                                            


let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_smart_case = 1
let g:deoplete#file#enable_buffer_path = 1
let g:deoplete#enable_ignore_case = 1
let g:deoplete#enable_smart_case = 1
let g:deoplete#sources#jedi#short_types = 1
let g:deoplete#auto_complete_delay = 150
let g:deoplete#sources#jedi#debug_server = 0
let g:deoplete#omni_patterns = {}
let g:deoplete#omni_patterns.php = '\h\w*\|[^. \t]->\%(\h\w*\)\?\|\h\w*::\%(\h\w*\)\?'
" let g:deoplate#omni_patterns.cs = '.*[^=\);]'

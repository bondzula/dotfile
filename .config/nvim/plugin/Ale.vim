"Vim plugin file
"Maintainer: Stefan Bondzulic
"Last Edit: 2017/07/26


         " (         
   " (     )\ )      
   " )\   (()/( (    
" ((((_)(  /(_)))\   
 " )\ _ )\(_)) ((_)  
 " (_)_\(_) |  | __| 
  " / _ \ | |__| _|  
 " /_/ \_\|____|___| 
                   


"Specify linters for a fietype
let g:ale_linters = {
  \ 'javascript':['eslint'],
  \ 'python' :['flake8'],
  \ 'html' :['tidy'],
  \ 'css' :['csslint']
  \}

let g:ale_lint_on_text_changed = 'never' "Dont run linters all the time, its to distracting
let g:ale_lint_on_insert_leave = 1
let g:ale_open_list = 1
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_sign_error = '✗'
let g:ale_sign_warning = '⚠'
let g:ale_statusline_format = ['✗ %d', '⚠ %d', '']  

let g:ale_set_loclist = 0
let g:ale_set_quickfix = 1

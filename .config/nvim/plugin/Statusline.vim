"Vim plugin file
"Maintainer: Stefan Bondzulic
"Last Edit: 2017/08/03



"  (                          (                  
"  )\ )   )        )          )\ )               
" (()/(( /(   ) ( /(  (      (()/((          (   
"  /(_))\()| /( )\())))\ (    /(_))\  (     ))\  
" (_))(_))/)(_)|_))//((_))\  (_))((_) )\ ) /((_) 
" / __| |_((_)_| |_(_))(((_) | |  (_)_(_/((_))   
" \__ \  _/ _` |  _| || (_-< | |__| | ' \)) -_)  
" |___/\__\__,_|\__|\_,_/__/ |____|_|_||_|\___|  
                                               


"If no errors are detected print "ok" otherwise print standard Ale message
function! LinterStatus() abort
    let l:counts = ale#statusline#Count(bufnr(''))
    return l:counts.total == 0 ? 'OK' : ALEGetStatusLine()
endfunction

"Print head
function! GitInfo()
  let git = fugitive#head()
  if git != ''
    return ' '.fugitive#head()
  else
    return ''
endfunction

set laststatus=2
set statusline=
set statusline+=%*
set statusline+=%9*\ %=                                                                  " Space
set statusline+=%8*\ %t

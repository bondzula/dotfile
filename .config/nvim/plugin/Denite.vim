"Vim plugin file
"Maintainer: Stefan Bondzulic
"Last Edit: 2017/07/26


 " (            ) (               
 " )\ )      ( /( )\ )  *   )     
" (()/(  (   )\()|()/(` )  /((    
 " /(_)) )\ ((_)\ /(_))( )(_))\   
" (_))_ ((_) _((_|_)) (_(_()|(_)  
 " |   \| __| \| |_ _||_   _| __| 
 " | |) | _|| .` || |   | | | _|  
 " |___/|___|_|\_|___|  |_| |___| 
                                


" customize ignore globs
call denite#custom#source('file_rec', 'matchers', ['matcher_fuzzy','matcher_ignore_globs'])
call denite#custom#filter('matcher_ignore_globs', 'ignore_globs',
      \ [
      \ '.git/', 'build/', '__pycache__/',
      \ 'images/', '*.o', '*.make',
      \ '*.min.*',
      \ 'img/', 'fonts/'])


"====================== Menu ===============================
" let s:menus = {}

" let s:menus.shell = {
"       \ 'description': 'Shell config'
"       \ }
" let s:menus.shell.file_candidates = [
"       \ ['tmux', '~/.tmux.conf'],
"       \ ['bash', '~/.bashrc'],
"       \ ['zsh', '~/.zshrc']
"       \ ]

" call denite#custom#var('menu', 'menus', s:menus)

"===================== Rg - Find and Grep ==================

" Ripgrep: https://github.com/BurntSushi/ripgrep
    call denite#custom#var('grep', 'command', ['rg'])
    call denite#custom#var('grep', 'recursive_opts', [])
    call denite#custom#var('grep', 'final_opts', ['.'])
    call denite#custom#var('grep', 'separator', ['--'])
    call denite#custom#var('grep', 'default_opts', ['--maxdepth', '8', '--vimgrep', '--no-heading'])
    call denite#custom#var('file_rec', 'command',  [
        \ 'rg', '--maxdepth', '8', '--no-line-number', '--fixed-strings', '--files-with-matches', '--hidden', '--follow', '.'])


call denite#custom#option('default', {
      \ 'prompt': '→',
      \ 'short_source_names': 1,
      \ 'reversed': 1,
      \ 'winheight' : 10,
      \ 'cursorline': 1,
      \ 'highlight_matched_char': 'italic'
      \ })

" ===================== Mapings ===============================

nmap <silent> <leader>p :Denite file_rec <Cr>
nmap <silent> <leader>b :Denite buffer -mode=normal<Cr>
nmap <silent> <leader>t :Denite tag<Cr>
nmap <silent> <leader>m :Denite menu -mode=normal <Cr>
nmap <silent> <leader>l :Denite line<Cr>
nmap <silent> <leader>g :Denite grep<Cr>
nmap <silent> <leader>f :Denite file<Cr>

call denite#custom#map('insert', "<C-j>", '<denite:move_to_next_line>')
call denite#custom#map('insert', "<C-k>", '<denite:move_to_previous_line>')
call denite#custom#map('insert', "<C-t>", '<denite:do_action:tabopen>')
call denite#custom#map('insert', "<C-v>", '<denite:do_action:vsplit>')
call denite#custom#map('normal', "v", '<denite:do_action:vsplit>')

"~ Buffer navigation
nnoremap [b :bprevious<cr> 
nnoremap ]b :bnext<cr>
nnoremap [B :bfirst<Cr>
nnoremap ]B :blast<Cr>

"~ Argument list navigation
nnoremap ]a :next<CR>
nnoremap [a :previous<CR>
nnoremap ]A :last<Cr>
nnoremap [A :first<Cr>

"~ Quick fix list
nnoremap ]q :cnext<Cr>
nnoremap [q :cprevious<Cr>
nnoremap ]Q :clast<Cr>
nnoremap [Q :cfirst<Cr>
nnoremap ]<C-q> :cnfile<Cr>
nnoremap [<C-q> :cpfile<Cr>

"~ Local list
nnoremap ]l :lnext<Cr>
nnoremap [l :lprevious<Cr>
nnoremap ]L :llast<Cr>
nnoremap [L :lfirst<Cr>
nnoremap ]<C-l> :lnfile<Cr>
nnoremap [<C-l> :lpfile<Cr>

"~ Tab settings
nnoremap ]t :tnext<Cr>
nnoremap [t :tprevious<Cr>
nnoremap ]T :tlast<Cr>
nnoremap [T :tfirst

"~ Insert lines above and below
nnoremap [k O<Esc>j
nnoremap ]k O<Esc>j
nnoremap [j o<Esc>k
nnoremap ]j o<Esc>k



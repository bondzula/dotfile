"Vim plugin file
"Maintainer: Stefan Bondzulic
"Last Edit: 2017/07/31
"TODO

"This is my personal vimrc that is subject to frequent changes


"         (      *    (
"         )\ ) (  `   )\ )  (
"  (   ( (()/( )\))( (()/(  )\
"  )\  )\ /(_)|(_)()\ /(_)|((_)
" ((_)((_|_)) (_()((_|_)) )\___
" \ \ / /|_ _||  \/  | _ ((/ __|
"  \ V /  | | | |\/| |   /| (__
"   \_/  |___||_|  |_|_|_\ \___|


"~ Table of conntent

      "Vim plug auto install ................................34
      "Plugins ..............................................41
      "Leader key ...........................................94
      "File type ............................................97
      "General ..............................................100
      "Interface ............................................112
      "Indentation ..........................................127
      "Search ...............................................135
      "Windows ..............................................141
      "Text .................................................147
      "File Navigation ......................................154
      "Scrolling ............................................157
      "Read / Write .........................................160

"~ Vim plug auto install
if empty(glob('~/.config/nvim/autoload/plug.vim'))
	silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
	\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"~ Plugins
call plug#begin('~/.config/nvim/plugged')

"Org mode
Plug 'jceb/vim-orgmode', { 'for' : 'org' } "Org mode plugin https://github.com/jceb/vim-orgmode
Plug 'vim-scripts/utl.vim', { 'for' : 'org'} "Universal Text Linking

"Colors
Plug 'morhetz/gruvbox'
Plug 'ayu-theme/ayu-vim'

"PHP
Plug 'StanAngeloff/php.vim'

"JavaScript
Plug 'pangloss/vim-javascript', {'for' : 'javascript'}
Plug 'othree/javascript-libraries-syntax.vim', {'for' : 'javascript'}
Plug 'elzr/vim-json', {'for' : 'json'}

"Web
Plug 'othree/html5.vim', {'for' : 'html'}
Plug 'hail2u/vim-css3-syntax', {'for' : 'css'}
Plug 'ekalinin/dockerfile.vim', {'for' : 'docker'}
Plug 'tpope/vim-dispatch'

"Navigation
Plug 'scrooloose/nerdtree', {'on' : 'NERDTreeToggle'}
Plug 'Shougo/denite.nvim' | Plug 'Shougo/neomru.vim'

"Utility's
Plug 'majutsushi/tagbar', {'on' : 'TagbarToggle'} "Displays tags in side view
Plug 'w0rp/ale', {'on' : 'ALEToggle'}                                                              " Asynchronous linter
Plug 'tpope/vim-commentary'                                                  " Comment toggle
Plug 'junegunn/vim-easy-align', { 'on': ['<Plug>(EasyAlign)', 'EasyAlign'] } " For aligning patterns

Plug 'tpope/vim-surround'                                                    " Surrounding manipulation
Plug 'tommcdo/vim-exchange'
Plug 'tpope/vim-repeat'
Plug 'junegunn/vim-peekaboo'
Plug 'lifepillar/vim-cheat40'
  let g:cheat40_use_default = 0

Plug 'junegunn/goyo.vim'
"Git
Plug 'tpope/vim-fugitive'

"Auto completion
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'mattn/emmet-vim', {'for': ['html', 'css', 'markdown', 'sass']}
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'carlitux/deoplete-ternjs', { 'for': 'javascript'}
Plug 'zchee/deoplete-jedi', {'for' : 'python'}


cal plug#end()

"~ Leader key
let mapleader="\<Space>"
let maplocalleader=","
"~ File type

set clipboard+=unnamedplus
filetype indent plugin on
syntax on

"~ General
set wildmenu
set wildignore+=*.o,*.rej,*/tmp/*,*.so,*.swp,*.zip "patterns to ignore during file navigaiton
set lazyredraw
set ffs=unix,dos,mac
set encoding=utf-8 nobomb
set virtualedit=block
set backspace=indent,eol,start
set history=500
set complete +=kspell "If spell is on, takse words from dictionary
"set synmaxcol=150 "For better performance????
set confirm

"~ Interface
let ayucolor="mirage" " for mirage version of theme
colorscheme ayu
set termguicolors     " enable true colors support
set background=dark
set signcolumn=yes
hi clear SignColumn
hi SpellBad cterm=bold,underline
call matchadd('ColorColumn', '\%81v', 100)
hi clear Folded

"~ Indentation
set autoindent
set smartindent
set smarttab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab

"~ Search
set incsearch
set hlsearch
set ignorecase
set smartcase
set infercase

"~ Windows
set splitbelow
set splitright
set nostartofline
set hidden
set switchbuf=usetab

"~ Text
set nowrap
set linebreak
set spellcapcheck=
set list
set listchars=tab:▸\ ,extends:❯,precedes:❮,nbsp:␣
set showbreak=↪

"~ File navigation
set path+=**
set suffixesadd=.js,.vim,.html,.py,.php

"~ Scrolling
set scrolloff=2
set sidescroll=2

"~ Read / Write
set nobackup
set nowritebackup
set noswapfile
set autowrite
set autoread

"Trail
set gdefault
set noshowmode

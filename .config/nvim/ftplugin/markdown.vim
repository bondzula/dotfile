" Vim filetype plugin
" Language:		Markdown
" Maintainer:		Tim Pope <vimNOSPAM@tpope.org>
" Last Change:		2016 Aug 29


   " *                                        
 " (  `               ) (                     
 " )\))(     ) (   ( /( )\ )    (  (          
" ((_)()\ ( /( )(  )\()|()/( (  )\))(   (     
" (_()((_))(_)|()\((_)\ ((_)))\((_)()\  )\ )  
" |  \/  ((_)_ ((_) |(_)_| |((_)(()((_)_(_/(  
" | |\/| / _` | '_| / / _` / _ \ V  V / ' \)) 
" |_|  |_\__,_|_| |_\_\__,_\___/\_/\_/|_||_|  
                                            


runtime! ftplugin/html.vim ftplugin/html_*.vim ftplugin/html/*.vim

setl comments=fb:*,fb:-,fb:+,n:> commentstring=>\ %s
setl formatoptions+=tcqln formatoptions-=r formatoptions-=o
setl formatlistpat=^\\s*\\d\\+\\.\\s\\+\\\|^[-*+]\\s\\+\\\|^\\[^\\ze[^\\]]\\+\\]:

if exists('b:undo_ftplugin')
  let b:undo_ftplugin .= "|setl cms< com< fo< flp<"
else
  let b:undo_ftplugin = "setl cms< com< fo< flp<"
endif

function! MarkdownFold()
  let line = getline(v:lnum)

  " Regular headers
  let depth = match(line, '\(^#\+\)\@<=\( .*$\)\@=')
  if depth > 0
    return ">" . depth
  endif

  " Setext style headings
  let nextline = getline(v:lnum + 1)
  if (line =~ '^.\+$') && (nextline =~ '^=\+$')
    return ">1"
  endif

  if (line =~ '^.\+$') && (nextline =~ '^-\+$')
    return ">2"
  endif

  return "="
endfunction

setl foldexpr=MarkdownFold()
setl foldmethod=expr
let b:undo_ftplugin .= " foldexpr< foldmethod<"


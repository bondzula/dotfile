"File type plugin
"Language: JavaScript
"Maintainer: Stefan Bondzulic 
"Last Edit: 07-25-2017 


"                      (                         
"                      )\ )                   )  
"    (     )  )      )(()/(    (  (        ( /(  
"    )\ ( /( /((  ( /( /(_)) ( )( )\ `  )  )\()) 
"   ((_))(_)|_))\ )(_)|_))   )(()((_)/(/( (_))/  
"  _ | ((_)__)((_|(_)_/ __| ((_|(_|_|(_)_\| |_   
" | || / _` \ V // _` \__ \/ _| '_| | '_ \)  _|  
"  \__/\__,_|\_/ \__,_|___/\__|_| |_| .__/ \__|  
"                                   |_|          


"Auto format comments and wrap them
setl formatoptions-=tl formatoptions+=croq
setl tw=79

"Set omni completion
setl omnifunc=javascriptcomplete#CompleteJS


"Folding 
setl foldmethod=marker
setl foldmarker={,}
setl nofoldenable
setl foldnestmax=3

"Tab settings
setl tabstop=2
setl shiftwidth=2

" Set 'comments' to format dashed lists in comments.
setl comments=sO:*\ -,mO:*\ \ ,exO:*/,s1:/*,mb:*,ex:*/,://
setl commentstring=//%s


let b:undo_ftplugin = "setl fo< ofu< com< cms< fdm< fdr< nofen< fdl< ts< sw<"

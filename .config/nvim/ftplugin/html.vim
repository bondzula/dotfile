"file type plugin
"Language: HTML
"Maintainer: Stefan Bondzulic 
"Last Edit: 07-25-2017 


"     )           *    (     
"  ( /(   *   ) (  `   )\ )  
"  )\())` )  /( )\))( (()/(  
" ((_)\  ( )(_)|(_)()\ /(_)) 
"  _((_)(_(_())(_()((_|_))   
" | || ||_   _||  \/  | |    
" | __ |  | |  | |\/| | |__  
" |_||_|  |_|  |_|  |_|____| 
                           


setl matchpairs+=<:>
setl commentstring=<!--%s-->
setl comments=s:<!--,m:\ \ \ \ ,e:-->

"Auto format and wrap comments
setl fo+=croq
setl fo-=l
setl tw=79

"folds
setl foldmethod=indent
setl foldnestmax=3
setl nofoldenable

"Omni completion
setl omnifunc=htmlcomplete#CompleteTags

" HTML:  thanks to Johannes Zellner and Benji Fisher.
if exists("loaded_matchit")
    let b:match_ignorecase = 1
    let b:match_words = '<:>,' .
    \ '<\@<=[ou]l\>[^>]*\%(>\|$\):<\@<=li\>:<\@<=/[ou]l>,' .
    \ '<\@<=dl\>[^>]*\%(>\|$\):<\@<=d[td]\>:<\@<=/dl>,' .
    \ '<\@<=\([^/][^ \t>]*\)[^>]*\%(>\|$\):<\@<=/\1>'
endif


" Undo the stuff we changed.
let b:undo_ftplugin = "setl commentstring< matchpairs< omnifunc< comments< formatoptions< tw< fdm< nofen< fdl<" .
    \	" | unlet! b:match_ignorecase b:match_words"


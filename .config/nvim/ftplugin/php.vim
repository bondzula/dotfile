"Vim plugin file
"Maintainer: Stefan Bondzulic
"Last Edit: 2017/07/26


 " (       ) (     
 " )\ ) ( /( )\ )  
" (()/( )\()|()/(  
 " /(_)|(_)\ /(_)) 
" (_))  _((_|_))   
" | _ \| || | _ \  
" |  _/| __ |  _/  
" |_|  |_||_|_|    
                 

setl include=\\\(require\\\|include\\\)\\\(_once\\\)\\\?
" Disabled changing 'iskeyword', it breaks a command such as "*"
" setl iskeyword+=$

if exists("loaded_matchit")
    let b:match_words = '<?php:?>,\<switch\>:\<endswitch\>,' .
		      \ '\<if\>:\<elseif\>:\<else\>:\<endif\>,' .
		      \ '\<while\>:\<endwhile\>,' .
		      \ '\<do\>:\<while\>,' .
		      \ '\<for\>:\<endfor\>,' .
		      \ '\<foreach\>:\<endforeach\>,' .
                      \ '(:),[:],{:},' .
		      \ s:match_words
endif

setl omnifunc=phpcomplete#CompletePHP

" Section jumping: [[ and ]] provided by Antony Scriven <adscriven at gmail dot com>
let s:function = '\(abstract\s\+\|final\s\+\|private\s\+\|protected\s\+\|public\s\+\|static\s\+\)*function'
let s:class = '\(abstract\s\+\|final\s\+\)*class'
let s:interface = 'interface'
let s:section = '\(.*\%#\)\@!\_^\s*\zs\('.s:function.'\|'.s:class.'\|'.s:interface.'\)'
exe 'nno <buffer> <silent> [[ ?' . escape(s:section, '|') . '?<CR>:nohls<CR>'
exe 'nno <buffer> <silent> ]] /' . escape(s:section, '|') . '/<CR>:nohls<CR>'
exe 'ono <buffer> <silent> [[ ?' . escape(s:section, '|') . '?<CR>:nohls<CR>'
exe 'ono <buffer> <silent> ]] /' . escape(s:section, '|') . '/<CR>:nohls<CR>'

setl commentstring=/*%s*/

" Undo the stuff we changed.
let b:undo_ftplugin = "setl commentstring< include< omnifunc<" .
	    \	      " | unlet! b:browsefilter b:match_words | " .
	    \	      s:undo_ftplugin

setl tabstop=4
setl shiftwidth=4

" filetype plugin file
" Lenguage: CSS
" Maintainer: Stefan Bondzulic
" Last Edit: 07-25-2017 


"       (   (     
"   (   )\ ))\ )  
"   )\ (()/(()/(  
" (((_) /(_))(_)) 
" )\___(_))(_))   
"((/ __/ __/ __|  
" | (__\__ \__ \  
"  \___|___/___/  


"Define undoes when filetype is changed
let b:undo_ftplugin = "setl com< cms< inc< fo< ofu< fdm< fmr< nofen< tw<"

"Auto format and wrap comments 
setl formatoptions+=croq
setl fo-=l
setl comments=s1:/*,mb:*,ex:*/ commentstring&
"Set completion
setl omnifunc=csscomplete#CompleteCSS
setl tw=79

let &l:include = '^\s*@import\s\+\%(url(\)\='

"Folding
setl foldmethod=marker
setl foldmarker={,}
setl nofoldenable

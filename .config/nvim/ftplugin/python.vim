"Vim plugin file
"Maintainer: Stefan Bondzulic
"Last Edit: 2017/07/26


 " (       )            )    )     )  
 " )\ ) ( /(   *   ) ( /( ( /(  ( /(  
" (()/( )\())` )  /( )\()))\()) )\()) 
 " /(_)|(_)\  ( )(_)|(_)\((_)\ ((_)\  
" (_))__ ((_)(_(_()) _((_) ((_) _((_) 
" | _ \ \ / /|_   _|| || |/ _ \| \| | 
" |  _/\ V /   | |  | __ | (_) | .` | 
" |_|   |_|    |_|  |_||_|\___/|_|\_| 
                                    


if exists("b:did_ftplugin") | finish | endif
let b:did_ftplugin = 1

setl cinkeys-=0#
setl indentkeys-=0#
setl include=^\\s*\\(from\\\|import\\)
setl includeexpr=substitute(v:fname,'\\.','/','g')
setl suffixesadd=.py
setl comments=b:#,fb:-
setl commentstring=#\ %s

setl omnifunc=python3complete#Complete

set wildignore+=*.pyc

let b:next_toplevel='\v%$\|^(class\|def\|async def)>'
let b:prev_toplevel='\v^(class\|def\|async def)>'
let b:next='\v%$\|^\s*(class\|def\|async def)>'
let b:prev='\v^\s*(class\|def\|async def)>'

execute "nnoremap <silent> <buffer> ]] :call <SID>Python_jump('n', '". b:next_toplevel."', 'W')<cr>"
execute "nnoremap <silent> <buffer> [[ :call <SID>Python_jump('n', '". b:prev_toplevel."', 'Wb')<cr>"
execute "nnoremap <silent> <buffer> ]m :call <SID>Python_jump('n', '". b:next."', 'W')<cr>"
execute "nnoremap <silent> <buffer> [m :call <SID>Python_jump('n', '". b:prev."', 'Wb')<cr>"

execute "onoremap <silent> <buffer> ]] :call <SID>Python_jump('o', '". b:next_toplevel."', 'W')<cr>"
execute "onoremap <silent> <buffer> [[ :call <SID>Python_jump('o', '". b:prev_toplevel."', 'Wb')<cr>"
execute "onoremap <silent> <buffer> ]m :call <SID>Python_jump('o', '". b:next."', 'W')<cr>"
execute "onoremap <silent> <buffer> [m :call <SID>Python_jump('o', '". b:prev."', 'Wb')<cr>"

execute "xnoremap <silent> <buffer> ]] :call <SID>Python_jump('x', '". b:next_toplevel."', 'W')<cr>"
execute "xnoremap <silent> <buffer> [[ :call <SID>Python_jump('x', '". b:prev_toplevel."', 'Wb')<cr>"
execute "xnoremap <silent> <buffer> ]m :call <SID>Python_jump('x', '". b:next."', 'W')<cr>"
execute "xnoremap <silent> <buffer> [m :call <SID>Python_jump('x', '". b:prev."', 'Wb')<cr>"

if !exists('*<SID>Python_jump')
  fun! <SID>Python_jump(mode, motion, flags) range
      if a:mode == 'x'
          normal! gv
      endif

      normal! 0

      let cnt = v:count1
      mark '
      while cnt > 0
          call search(a:motion, a:flags)
          let cnt = cnt - 1
      endwhile

      normal! ^
  endfun
endif

if has("browsefilter") && !exists("b:browsefilter")
    let b:browsefilter = "Python Files (*.py)\t*.py\n" .
                \ "All Files (*.*)\t*.*\n"
endif

setl expandtab shiftwidth=4 softtabstop=4 tabstop=8

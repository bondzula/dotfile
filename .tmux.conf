# Address vim mode switching delay
set -s escape-time 0

# Increase scrollback buffer size
set -g history-limit 50000

# Tmux messages are displayed for 2 seconds
set -g display-time 4000

# Refresh 'status-left' and 'status-interval' more often
set -g status-interval 5

# Upgrade $TERM
set -s default-terminal "screen-256color"

# vi binding for copy mode
setw -g mode-keys vi

# Focus events enabled for terminals that support them
set -g focus-events on

# super useful when using "grouped sessions" and multi-monitor setup
setw -g aggressive-resize on

#Set prefix to space
unbind C-b
set -g prefix C-Space

unbind c
bind c new-window -c '#{pane_current_path}'

# Resize pane
unbind Left
bind -r Left resize-pane -L 5
unbind Right
bind -r Right resize-pane -R 5
unbind Down
bind -r Down resize-pane -D 5
unbind Up
bind -r Up resize-pane -U 5

# Move between panes
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# Move pane
bind J swap-pane -D
bind K swap-pane -U

# Kill pane
unbind c
bind c kill-pane

# New window
unbind n
bind n new-window

# Navigate windows
bind ] next-window
bind [ previous-window

# New split pane
bind v split-window -h -c '#{pane_current_path}'
bind s split-window -v -c '#{pane_current_path}'

# Copy mode
bind-key Escape copy-mode
# bind-key -T copy-mode-vi Escape cancel

# New 2.4 version:
bind-key -T copy-mode-vi v send -X begin-selection
bind-key -T copy-mode-vi C-v send -X rectangle-toggle
bind-key -T copy-mode-vi V send -X selet-line
bind-key -T copy-mode-vi y send -X copy-pipe-and-cancel "wl-copy"

#Start window and pane numbering at 1
set -g base-index 1
set -g pane-base-index 1

#Dont wait for an escape sequence after seeing C-a
set -sg escape-time 0

#But dont change tmux's own window titles
set -w -g automatic-rename off

# source .tmux.conf file
bind r source-file ~/.tmux.conf \; display "Configuration Reloaded!"

set -g status off
set -g status-fg colour39
set -g status-bg default
set -g status-left "#(echo '#{pane_current_command}')"
setw -g window-status-format ""
setw -g window-status-current-format ""
set -g status-right "#[bold]%d/%m %H:%M:%S"

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-open'
set -g @plugin 'tmux-plugins/tmux-sessionist'
set -g @plugin 'tmux-plugins/tmux-open'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'

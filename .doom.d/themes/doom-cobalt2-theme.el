;;;  -*- lexical-binding: t; -*-
;;; +theme|doom-gruvbox.el --- Gruvbox theme

(require 'doom-themes)

(defgroup doom-cobalt2-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-cobalt2-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-cobalt2-theme
  :type 'boolean)

(defcustom doom-cobalt2-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-cobalt2-theme
  :type 'boolean)

(defcustom doom-cobalt2-comment-bg doom-cobalt2-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'doom-cobalt2-theme
  :type 'boolean)

(defcustom doom-cobalt2-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-cobalt2-theme
  :type '(or integer boolean))

(defcustom doom-cobalt2-region-highlight t
  "Determines the selection highlight style. Can be 'frost, 'snowstorm or t
(default)."
  :group 'doom-cobalt2-theme
  :type 'symbol)

(def-doom-theme doom-cobalt2
  "A dark gruvbox theme."

  ;; name        default   256       16
  ((bg         '("#193549" nil       nil))
   (bg-alt     '("#193549" nil       nil))
   (base0      '("#000000" "black"   "black"))
   (base1      '("#3c3836" "#3c3836" "brightblack"))
   (base2      '("#504945" "#504945" "brightblack"))
   (base3      '("#665c54" "#665c54" "brightblack"))
   (base4      '("#3f3f3f" "#7c6f64" "brightblack"))
   (base5      '("#a89984" "#a89984" "brightblack"))
   (base6      '("#bdae93" "#bdae93" "brightblack"))
   (base7      '("#d5c4a1" "#d5c4a1" "brightblack"))
   (base8      '("#ebdbb2" "#ebdbb2" "white"))
   (fg         '("#ebdbb2" "#dfdfdf" "white"))
   (fg-alt     '("#fbf1c7" "#bfbfbf" "brightwhite"))

   (grey       '("#a89984" "#928374" "base2"))
   (red        '("#fb4934" "#cc241d" "red"))
   (orange     '("#ff9d00" "#fe8019" "brightred"))
   (green      '("#98971a" "#b8bb26" "green"))
   (teal       '("#8EBCBB" "#44b9b1" "brightgreen"))
   (yellow     '("#ffc600" "#ECBE7B" "yellow"))
   (blue       '("#0050A4" "#83a598" "brightblue"))
   (dark-blue  '("#458588" "#2257A0" "blue"))
   (magenta    '("#b16286" "#d3869b" "purple"))
   (violet     '("#5D80AE" "#a9a1e1" "brightmagenta"))
   (cyan       '("#86C0D1" "#46D9FF" "brightcyan"))
   (dark-cyan  '("#507681" "#5699AF" "cyan"))
   (aqua       '("#689d6a" "#8ec07c" "aqua"))


   ;; face categories -- required for all themes
   (background    bg)
   (highlight     "#0050A4")
   (vertical-bar  "#0d3a58")
   (selection     "#0050A4")
   (builtin        cyan)
   (comments      "#0280f0")
   (doc-comments   (doom-lighten (if doom-cobalt2-brighter-comments "#0050A4" base5) 0.25))
   (constants     "#ff628c")
   (functions     "#ff9d00")
   (keywords      "#ffc600")
   (methods        cyan)
   (operators     "#ff9d00")
   (type           yellow)
   (strings       "#a5ff90")
   (variables     "#fff")
   (numbers        cyan)
   (region         base4)
   (error         "#a22929")
   (warning       "#ffc600")
   (success       "#3c9f4a")
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg) "black" "black"))
   (-modeline-bright doom-cobalt2-brighter-modeline)
   (-modeline-pad
    (when doom-cobalt2-padded-modeline
      (if (integerp doom-cobalt2-padded-modeline) doom-cobalt2-padded-modeline 4)))

   (modeline-fg     nil)
   (modeline-fg-alt base5)

   (modeline-bg
    (if -modeline-bright
        (doom-darken "#0050A4" 0.475)
      `(,(doom-darken (car bg-alt) 0.15) ,@(cdr base0))))
   (modeline-bg-l
    (if -modeline-bright
        (doom-darken "#0050A4" 0.45)
      `(,(doom-darken (car bg-alt) 0.1) ,@(cdr base0))))
   (modeline-bg-inactive   (doom-darken bg-alt 0.1))
   (modeline-bg-inactive-l `(,(car bg-alt) ,@(cdr base1))))

  ;; --- extra faces ------------------------
  (
   ;; --- base faces -------------------------
   (default              :background "#193549" :foreground "#fff")
   (fringe               :background "#122738")                                ;; TODO doesn't work
   (region               :background "#0050a4")                                ;; Visual selection
   (highlight            :background "#6c6a2e" :box "#47523a")                 ;; Not complity sure, hover over links
   (cursor               :background "#ffc600")
   (shadow               :foreground "#3f3f3f")
   (minibuffer-prompt    :foreground highlight)                                ;; TODO
   (tooltip              :background base3 :foreground fg)                     ;; TODO
   (secondary-selection  :background "#0050a4")                                ;; TODO
   (lazy-highlight       :background "#0050a4" :distant-foreground "#6c6a2e")  ;; Search Highlight
   (match                :background "#6c6a2e")                                ;; TODO Matching stuff, perenthases
   (trailing-whitespace  :background red)
   (vertical-border      :background "#0d3a58" :foreground "#0d3a58")
   (link                 :foreground highlight :underline t :weight 'normal)
   (line-number          :background "#193549" :foreground "#aaa")

   ;; Plugins
   (org-level-1                :background nil :foreground "#9effff" :weight 'bold :height 140)
   (org-level-2                :background nil :foreground "#9effff" :weight 'bold :height 130)
   (org-level-3                :background nil :foreground "#9effff" :weight 'bold :height 120)
   (org-level-4                :background nil :foreground "#9effff" :weight 'bold :height 110)
   (org-level-5                :background nil :foreground "#9effff" :weight 'bold :height 110)
   (org-level-6                :background nil :foreground "#9effff" :weight 'bold :height 110)
   (org-level-7                :background nil :foreground "#9effff" :weight 'bold :height 110)
   (org-level-8                :background nil :foreground "#9effff" :weight 'bold :height 110)
   (org-document-info-keyword  :foreground "#ebdbb2")
   (org-meta-line              :slant 'italic)
   (org-link                   :foreground "#9effff" :weight 'bold :underline t)
   (org-tag :foreground "#0280f0")

   (org-todo :foreground "#FF6C6B")
   (org-done :foreground "#ffc600")

   (rainbow-delimiters-depth-1-face :foreground "#ffc600")
   (hl-line :background "#1f4662") ;; line highlight

   (solaire-default-face :background "#122738")
   (solaire-line-nubmer-face :background "#193549")

   (mmm-default-submode-face :background "#193549")

   ;; js2
   (js2-function-call :foreground "#ff9d00")
   (js2-object-property :foreground "#9effff")
   (js2-object-property-access :foreground "#6BD6A3")

   ;; css
   (css-selector :foreground "#3ad900")

   ;; web mode
   (web-mode-html-tag-bracket-face :foreground "#D5E4F5")
   (web-mode-html-tag-face :foreground "#9EFFFF")
   (web-mode-html-attr-equal-face :foreground "#9effff")
   (web-mode-css-property-name-face :foreground "#98EC89")
   (web-mode-css-selector-face :foreground "#3AD900")
   (web-mode-css-function-face :foreground "#E69107")

   ;; --- End Stefan
   ((line-number &override) :foreground fg-alt)
   ((line-number-current-line &override) :foreground fg)


   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))

   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis
    :foreground (if -modeline-bright base8 highlight))


   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l)))))


(add-to-list 'default-frame-alist '(ns-appearance . dark))

(push '(doom-cobalt2 . t) +doom-solaire-themes)

;;; +theme|doom-gruvbox.el ends here

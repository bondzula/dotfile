;;; doom-ayu-dark-theme.el --- inspired by Atom One Dark
(require 'doom-themes)

;;
(defgroup doom-ayu-dark-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-ayu-dark-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-ayu-dark-theme
  :type 'boolean)

(defcustom doom-ayu-dark-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-ayu-dark-theme
  :type 'boolean)

(defcustom doom-ayu-dark-comment-bg doom-ayu-dark-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'doom-ayu-dark-theme
  :type 'boolean)

(defcustom doom-ayu-dark-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-ayu-dark-theme
  :type '(or integer boolean))

;;
(def-doom-theme doom-ayu-dark
  "A dark theme inspired by Atom One Dark"

  ;; name        default   256       16
  ((bg         '("#1f2430" nil       nil))
   (bg-alt     '("#232834" nil       nil))
   (base0      '("#191e2a" "black"   "black"))
   (base1      '("#1c1f24" "#1e1e1e" "brightblack"))
   (base2      '("#202328" "#2e2e2e" "brightblack"))
   (base3      '("#23272e" "#262626" "brightblack"))
   (base4      '("#3f444a" "#3f3f3f" "brightblack"))
   (base5      '("#5B6268" "#525252" "brightblack"))
   (base6      '("#73797e" "#6b6b6b" "brightblack"))
   (base7      '("#9ca0a4" "#979797" "brightblack"))
   (base8      '("#DFDFDF" "#dfdfdf" "white"))
   (fg         '("#cbccc6" "#bfbfbf" "brightwhite"))
   (fg-alt     '("#d5d6d1" "#2d2d2d" "white"))

   (grey       base4)
   (red        '("#ed8274" "#ff6655" "red"))
   (orange     '("#ffa759" "#dd8844" "brightred"))
   (green      '("#a6cc70" "#99bb66" "green"))
   (teal       '("#4db5bd" "#44b9b1" "brightgreen"))
   (yellow     '("#fad07b" "#ECBE7B" "yellow"))
   (blue       '("#73d0ff" "#51afef" "brightblue"))
   (dark-blue  '("#6dcbfa" "#2257A0" "blue"))
   (magenta    '("#cfbafa" "#c678dd" "brightmagenta"))
   (violet     '("#d4bfff" "#a9a1e1" "magenta"))
   (cyan       '("#95e6cb" "#46D9FF" "brightcyan"))
   (dark-cyan  '("#90e1c6" "#5699AF" "cyan"))

   ;; face categories -- required for all themes
   (highlight      blue)
   (vertical-bar   (doom-darken base1 0.1))
   (selection      dark-blue)
   (builtin        orange)
   (comments       (if doom-ayu-dark-brighter-comments dark-cyan base5))
   (doc-comments   (doom-lighten (if doom-ayu-dark-brighter-comments dark-cyan base5) 0.25))
   (constants      violet)
   (functions      yellow)
   (keywords       orange)
   (methods        cyan)
   (operators      blue)
   (type           blue)
   (strings        green)
   (variables      fg)
   (numbers        orange)
   (region         `(,(doom-lighten (car bg-alt) 0.15) ,@(doom-lighten (cdr base0) 0.35)))
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg) "black" "black"))
   (-modeline-bright doom-ayu-dark-brighter-modeline)
   (-modeline-pad
    (when doom-ayu-dark-padded-modeline
      (if (integerp doom-ayu-dark-padded-modeline) doom-ayu-dark-padded-modeline 4)))

   (modeline-fg     nil)
   (modeline-fg-alt base5)

   (modeline-bg
    (if -modeline-bright
        (doom-darken blue 0.475)
      `(,(doom-darken (car bg-alt) 0.15) ,@(cdr base0))))
   (modeline-bg-l
    (if -modeline-bright
        (doom-darken blue 0.45)
      `(,(doom-darken (car bg-alt) 0.1) ,@(cdr base0))))
   (modeline-bg-inactive   `(,(doom-darken (car bg-alt) 0.1) ,@(cdr bg-alt)))
   (modeline-bg-inactive-l `(,(car bg-alt) ,@(cdr base1))))


  ;; --- extra faces ------------------------
  ((elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")

   (evil-goggles-default-face :inherit 'region :background (doom-blend region bg 0.5))

   ((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground fg)

   (font-lock-comment-face
    :foreground comments
    :background (if doom-ayu-dark-comment-bg (doom-lighten bg 0.05)))
   (font-lock-doc-face
    :inherit 'font-lock-comment-face
    :foreground doc-comments)

   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis
    :foreground (if -modeline-bright base8 highlight))

   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l)))

   ;; Doom modeline
   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))
   (doom-modeline-buffer-file :inherit 'mode-line-buffer-id :weight 'bold)
   (doom-modeline-buffer-path :inherit 'mode-line-emphasis :weight 'bold)
   (doom-modeline-buffer-project-root :foreground green :weight 'bold)

   ;; ivy-mode
   (ivy-current-match :background base4 :distant-foreground base0 :weight 'normal)
   (ivy-minibuffer-match-face-2 :foreground yellow)
   (ivy-minibuffer-match-face-3 :weight 'semi-bold :foreground yellow)
   (ivy-minibuffer-match-face-4 :weight 'semi-bold :foreground yellow)

   ;; Search Highlight
   (evil-ex-lazy-highlight :background base4 :distant-foregorund base0 :weight 'normal)
   (lsp-face-highlight-read :background base4 :distant-foregorund base0 :weight 'normal)

   ;; --- major-mode faces -------------------
   ;; css-mode / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)

   ;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   (markdown-code-face :background (doom-lighten base3 0.05))

   ;; php-mode
   (php-doc-annotation-tag :foreground orange)
   (php-$this-sigil :foreground blue)
   (php-$this :foreground blue :italic t)
   (php-method-call :foreground yellow)
   (php-php-tag :foreground fg)

   ;; org-mode
   (org-level-1 :weight 'semi-bold :foreground fg-alt)
   (org-level-2 :weight 'semi-bold :foreground fg-alt)
   (org-level-3 :weight 'semi-bold :foreground fg-alt)
   (org-level-4 :weight 'semi-bold :foreground fg-alt)
   (org-level-5 :weight 'semi-bold :foreground fg-alt)
   (org-level-6 :weight 'semi-bold :foreground fg-alt)
   (org-level-7 :weight 'semi-bold :foreground fg-alt)
   (org-level-8 :weight 'semi-bold :foreground fg-alt)
   (org-link :foreground blue :underline t)
   (org-hide :foreground hidden)
   (solaire-org-hide-face :foreground hidden)
   (org-todo :foreground red)
   (org-done :foreground green)))



  ;; --- extra variables ---------------------
  ;; ()


;;; doom-ayu-dark-theme.el ends here

;;;  -*- lexical-binding: t; -*-

;;; +theme|doom-gruvbox.el --- Gruvbox theme

(require 'doom-themes)

(defgroup doom-gruvbox-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-gruvbox-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-gruvbox-theme
  :type 'boolean)

(defcustom doom-gruvbox-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-gruvbox-theme
  :type 'boolean)

(defcustom doom-gruvbox-comment-bg doom-gruvbox-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'doom-gruvbox-theme
  :type 'boolean)

(defcustom doom-gruvbox-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-gruvbox-theme
  :type '(or integer boolean))

(defcustom doom-gruvbox-region-highlight t
  "Determines the selection highlight style. Can be 'frost, 'snowstorm or t
(default)."
  :group 'doom-gruvbox-theme
  :type 'symbol)

(def-doom-theme doom-gruvbox
  "A dark gruvbox theme."

  ;; name        default   256       16
  ((bg     '("#282828" nil       nil))
   (bg-alt '("#252525" nil       nil))
   (base0  '("#000000" "black"   "black"))
   (base1  '("#3c3836" "#3c3836" "brightblack"))
   (base2  '("#504945" "#504945" "brightblack"))
   (base3  '("#665c54" "#665c54" "brightblack"))
   (base4  '("#3f3f3f" "#7c6f64" "brightblack"))
   (base5  '("#a89984" "#a89984" "brightblack"))
   (base6  '("#bdae93" "#bdae93" "brightblack"))
   (base7  '("#d5c4a1" "#d5c4a1" "brightblack"))
   (base8  '("#ebdbb2" "#ebdbb2" "white"))
   (fg     '("#ebdbb2" "#dfdfdf" "white"))
   (fg-alt '("#fbf1c7" "#bfbfbf" "brightwhite"))

   (grey   '("#a89984" "#928374" "base2"))
   (red    '("#fb4934" "#cc241d" "red"))
   (orange '("#d65d0e" "#fe8019" "brightred"))
   (green  '("#98971a" "#b8bb26" "green"))
   (teal   '("#8EBCBB" "#44b9b1" "brightgreen"))
   (yellow '("#fabd2f" "#ECBE7B" "yellow"))
   (blue   '("#458588" "#83a598" "brightblue"))
   (dark-blue  '("#458588" "#2257A0" "blue"))
   (magenta    '("#b16286" "#d3869b" "purple"))
   (violet     '("#5D80AE" "#a9a1e1" "brightmagenta"))
   (cyan       '("#86C0D1" "#46D9FF" "brightcyan"))
   (dark-cyan  '("#507681" "#5699AF" "cyan"))
   (aqua   '("#689d6a" "#8ec07c" "aqua"))

   ;; face categories -- required for all themes
   (highlight      base5)
   (vertical-bar   (doom-darken bg-alt 0.4))
   (selection      cyan)
   (builtin        cyan)
   (comments       (if doom-gruvbox-brighter-comments blue (doom-lighten base5 0.2)))
   (doc-comments   (doom-lighten (if doom-gruvbox-brighter-comments blue base5) 0.25))
   (constants      cyan)
   (functions      green)
   (keywords       red)
   (methods        cyan)
   (operators      cyan)
   (type           yellow)
   (strings        green)
   (variables      fg)
   (numbers        cyan)
   (region         base4)
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg) "black" "black"))
   (-modeline-bright doom-gruvbox-brighter-modeline)
   (-modeline-pad
    (when doom-gruvbox-padded-modeline
      (if (integerp doom-gruvbox-padded-modeline) doom-gruvbox-padded-modeline 4)))

   (modeline-fg     nil)
   (modeline-fg-alt base5)

   (modeline-bg
    (if -modeline-bright
        (doom-darken blue 0.475)
      `(,(doom-darken (car bg-alt) 0.15) ,@(cdr base0))))
   (modeline-bg-l
    (if -modeline-bright
        (doom-darken blue 0.45)
      `(,(doom-darken (car bg-alt) 0.1) ,@(cdr base0))))
   (modeline-bg-inactive   (doom-darken bg-alt 0.1))
   (modeline-bg-inactive-l `(,(car bg-alt) ,@(cdr base1))))

  ;; --- extra faces ------------------------
  (
   ((line-number &override) :foreground fg-alt)
   ((line-number-current-line &override) :foreground fg)

   (font-lock-comment-face
    :foreground comments
    :background (if doom-gruvbox-comment-bg (doom-lighten bg 0.05)))
   (font-lock-doc-face
    :inherit 'font-lock-comment-face
    :foreground doc-comments)

   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))

   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis
    :foreground (if -modeline-bright base8 highlight))

   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l)))))

(add-to-list 'default-frame-alist '(ns-appearance . dark))

(push '(doom-gruvbox . t) +doom-solaire-themes)

;;; +theme|doom-gruvbox.el ends here

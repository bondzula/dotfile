;;; ~/.doom.d/mapings.el -*- lexical-binding: t; -*-

(map!

 :inv "<f12>" #'+lookup/definition
 :inv "<S-f12>" #'+lookup/references
 :inv "<f2>" #'lsp-rename

 :inv "<M-up>" #'+default/move-line-up
 :inv "<M-down>" #'+default/move-line-down

 :nv "C-M-j" #'evil-mc-make-cursor-move-next-line
 :nv "C-M-k" #'evil-mc-make-cursor-move-prev-line
 :nv "C-M-a" #'evil-mc-make-all-cursors

 :inv "C-s" #'save-buffer

 ;; Search with swiper
 :nv "/" #'swiper
 :nv "C-/" #'swiper-all

 :leader (:prefix ("f" . "File")
           :desc "Rename file" :vn "R" #'rename-file-and-buffer
           :desc "New file"    :vn "n" #'find-file)

         (:prefix ("e" . "Edit")
           :desc "Align" :v "a" #'align-regexp)

         (:prefix "f"
           :desc "Find file in config" :n "t" #'+default/find-in-config)

 (:after treemacs-evil
   :map evil-treemacs-state-map
   "C-h" #'evil-window-left
   "C-l" #'evil-window-right)

 :localleader
 :map org-mode-map
 "f" #'org-match-sparse-tree
 "r" #'org-refile
 "m" #'org-set-tags-command)

;;; .doom.d/config.el -*- lexical-binding: t; -*-
(map!

 :inv "<f12>" #'+lookup/definition
 :inv "<S-f12>" #'+lookup/references
 :inv "<f2>" #'lsp-rename

 :inv "<M-up>" #'+default/move-line-up
 :inv "<M-down>" #'+default/move-line-down

 :nv "C-M-j" #'evil-mc-make-cursor-move-next-line
 :nv "C-M-k" #'evil-mc-make-cursor-move-prev-line
 :nv "C-M-a" #'evil-mc-make-all-cursors

 :inv "C-s" #'save-buffer

 ;; Search with swiper
 :nv "/" #'swiper
 :nv "C-/" #'swiper-all

 :leader (:prefix ("t" . "Toggle")
           :desc "MPD Playback"    :vn "m" #'libmpdel-playback-play-pause)

         (:prefix ("e" . "Edit")
           :desc "Align" :v "a" #'align-regexp)

 (:after treemacs-evil
   :map evil-treemacs-state-map
   "C-h" #'evil-window-left
   "C-l" #'evil-window-right))

(def-package! mpdel)

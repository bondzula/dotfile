;;; user/vue/config.el -*- lexical-binding: t; -*-

(use-package! vue-mode
  :commands vue-mode
  :mode "\\.vue\\'"
  :config
  (flycheck-add-mode 'javascript-eslint 'vue-mode)
  (flycheck-add-mode 'javascript-eslint 'vue-html-mode)
  (flycheck-add-mode 'javascript-eslint 'css-mode))


(custom-set-faces
 '(mmm-default-submode-face ((t nil))))

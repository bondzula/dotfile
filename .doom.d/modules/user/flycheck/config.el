;;; user/flycheck/config.el -*- lexical-binding: t; -*-

;; Replace js-hint with eslint for web-mode
;; Disable json-jsonlist checking for json files
(after! flycheck
  (setq-default flycheck-disabled-checkers
                (append flycheck-disabled-checkers
                        '(javascript-jshint)))
  (setq-default flycheck-disabled-checkers
                (append flycheck-disabled-checkers
                        '(json-jsonlist)))
  (setq flycheck-check-syntax-automatically '(save))
  (flycheck-add-mode 'javascript-eslint 'web-mode))


;; Use local eslint from node_modules instead of the global one
(defun +use-eslint-from-node-modules ()
  (let* ((root (locate-dominating-file
                (or (buffer-file-name) default-directory)
                "node_modules"))
         (eslint (and root
                      (expand-file-name "node_modules/eslint/bin/eslint.js"
                                        root))))
    (when (and eslint (file-executable-p eslint))
      (setq-local flycheck-javascript-eslint-executable eslint))))

(add-hook 'flycheck-mode-hook #'+use-eslint-from-node-modules)


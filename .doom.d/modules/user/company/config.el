;;; user/company/config.el -*- lexical-binding: t; -*-

;; Fine tune company autocompletion
(setq company-idel-delay 0.3
      company-minimum-prefix-length 3
      company-show-numbers t
      company-frontends '(company-tng-frontend
                          company-pseudo-tooltip-frontend
                          company-echo-metadata-frontend))

;;; user/interface/config.el -*- lexical-binding: t; -*-

;; Set doom theme
(setq doom-theme 'doom-ayu-dark)

;; Make emacs start maximized
(custom-set-variables
 '(initial-frame-alist (quote ((fullscreen . maximized)))))

;; Remove window title-bar
(set-frame-parameter nil 'undecorated t)

;; Set doom fonts
(setq doom-font (font-spec :family "Iosevka Term SS04" :size 15)
      doom-big-font (font-spec :family "Iosevka Term SS04" :size 18))

;;; user/org-mode/config.el -*- lexical-binding: t; -*-

;; Include org mode built-in modules
(setq org-modules '(org-drill org-habit))

;; Settup files and folders
(setq org-directory (substitute-in-file-name "$HOME/Documents/Notes/")
      org-archive-location (concat org-directory "archive.org::")
      org-agenda-files (list (concat org-directory "todos.org"
                              (concat org-directory "projects.org"))))

(after! org
  ;; Org mode mapings
  (map! :localleader
        :map org-mode-map
          :desc "Refile" "r" #'org-refile
          :desc "Schedule" "s" #'org-schedule
          :desc "Deadline" "d" #'org-deadline
          :desc "Go to heading" "/" #'counsel-org-goto
          :desc "Archive Subtree" "a" #'org-archive-subtree
          :desc "Export to" "e" #'org-export-dispatch
          :desc "Tag Heading" "t" #'org-set-tags-command)

  ;; Common ui settings
  (setq org-bullets-bullet-list '("⁖")
        org-hide-emphasis-markers 't
        org-ellipsis "  "
        org-blank-before-new-entry 't
        org-agenda-window-setup 'current-window)

  ;; Settup todo keywords
  (setq org-todo-keywords '((sequence "[ ](T)" "|" "[X](D)")
                            (sequence "TODO(t)" "|" "DONE(d)")
                            (sequence "WAITING(w)" "|" "STARTED(s)" "|" "CANCELLED(c)")))

  ;; Settup common tags
  (setq org-tag-alist '((:startgroup . nil) ("personal" . ?p) ("work" . ?w) (:endgroup . nil)
                        (:startgroup . nil) ("bug" . ?b) ("fixed" . ?f) ("wip" . ?i) (:endgroup . nil))))

;; Improved agenda views
(def-package! org-super-agenda
  :after org-agenda
  :init (setq org-super-agenda-groups
              '((:name "Today"
                       :time-grid t)
                (:name "Started" :todo "STARTED")
                (:name "Important" :priority "A" :order 1)
                (:todo "WAITING" :order 9)))
  :config (org-super-agenda-mode))

;;; user/general/config.el -*- lexical-binding: t; -*-

;; Set email and full name
(setq user-email-address "stefanbondzulic@gmail.com"
      user-full-name "Stefan Bondzulic")

;; Move files to trash on delete
(setq delete-by-moving-to-trash t)

;; Enable subword mode
(global-subword-mode +1)

;; Mappings
(map! (:when (featurep! :ui popup)
        :niv "C-`"   #'+popup/toggle))

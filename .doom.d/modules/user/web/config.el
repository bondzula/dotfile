;;; user/web-mode/config.el -*- lexical-binding: t; -*-

;;
;; web-mode
;;
(after! web-mode
  (setq web-mode-markup-indent-offset 2 ;; Indentation
        web-mode-code-indent-offset 0
        web-mode-enable-auto-quoting nil ;; disbale adding "" after an =
        web-mode-auto-close-style 0))

(add-hook 'web-mode-hook 'eslintd-fix-mode)

;;
;; lsp-ui
;;
(after! lsp-ui
  (progn
    (flycheck-add-mode 'javascript-eslint 'web-mode)
    (flycheck-add-next-checker 'lsp-ui '(warning . javascript-eslint)))

  (defun lsp-ui-flycheck-enable (_))
  "Enable flycheck integration for the current buffer."
  (when lsp-ui-flycheck-live-reporting
    (setq-local flycheck-check-syntax-automatically '(mode-started save)))
  (setq-local flycheck-checker 'lsp-ui)
  (lsp-ui-flycheck-add-mode major-mode)
  (add-to-list 'flycheck-checkers 'lsp-ui)
  (add-hook 'lsp-after-diagnostics-hook 'lsp-ui-flycheck--report nil t))
